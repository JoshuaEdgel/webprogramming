import { Component, OnInit, HostBinding, Input } from '@angular/core';
import {ISocketMessage } from '../interfaces';

@Component({
  selector: '[app-message]',
  templateUrl: './message.component.html',
  styleUrls: [`
    .username{
      font-weight:700;
      overflow: hidden:
      padding-right: 15px;
      text-align: right;
    }
  `]
})
export class MessageComponent implements OnInit {
  @input('app-message') public appMessage: ISocketMessage;
  @HostBinding('class.log') public isLog: boolean;
  @HostBinding('class.message') public isMessage: boolean;
  public color: string;
  public mesage: string;
  public username: string;

  constructor() { }

  ngOnInit() {
    this.isLog = this.appMessage.isLog;
    this.isMessage = !this.appMessage.isLog;
    this.color = this.appMessage.color;
    this.message = this.appMessage.message;
    this.username = this.appMessage.username;
  }

}
