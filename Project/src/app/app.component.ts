import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
   templateUrl: './app.component.html',
   styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Project';
}

function getElement<T extends HTMLElement>(id: string): T{
  const element = document.getElementById(id);

  if(element) {
      return element as T;
  }

  throw new Error(`Element with id ${id} ws not found`);
}

function rawDefense(a: number, b: number, c: number, d: number, e:number){
  a = getElement<HTMLInputElement>("hd").valueAsNumber;
  b = getElement<HTMLInputElement>("bd").valueAsNumber;
  c = getElement<HTMLInputElement>("ad").valueAsNumber;
  d = getElement<HTMLInputElement>("wd").valueAsNumber;
  e = getElement<HTMLInputElement>("ld").valueAsNumber;

  const answer = getElement<HTMLSpanElement>("answer");
  answer.innerText = `${((a + b + c + d + e) + 80)/80}`;
  return answer;
}


function elemDefense(a: number, b: number, c: number, d: number, e:number, f:number){
  //a = rawDefense(getElement<HTMLInputElement>("hd").valueAsNumber,getElement<HTMLInputElement>("bd").valueAsNumber,getElement<HTMLInputElement>("ad").valueAsNumber,getElement<HTMLInputElement>("wd").valueAsNumber,getElement<HTMLInputElement>("ld").valueAsNumber);
  b = getElement<HTMLInputElement>("hf").valueAsNumber;
  c = getElement<HTMLInputElement>("bf").valueAsNumber;
  d = getElement<HTMLInputElement>("af").valueAsNumber;
  e = getElement<HTMLInputElement>("wf").valueAsNumber;
  f = getElement<HTMLInputElement>("lf").valueAsNumber;

  const answerelem = getElement<HTMLSpanElement>("answer2");
  answerelem.innerText = `${a * ((1-(b + c + d + e + f))/100)}`;
  return answerelem;
}